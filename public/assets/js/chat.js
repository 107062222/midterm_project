
function init(){
    Submit = document.getElementById("submit");
    Txt = document.getElementById("txt");

    Submit.addEventListener('click', function(){
        if(Txt.value != ""){
            var user = firebase.auth().currentUser;
            firebase.database().ref('text/').push({
                name: user.email,
                data: Txt.value
            });
            Txt.value = "";
        }
    })

    var postsRef = firebase.database().ref('text');
    postsRef.on('child_added', function(data) {
            var parent = document.getElementById("post_list");
            var div = document.createElement("div");
            
            div.style.margin = "1px";
            div.style.backgroundColor = "white";
            div.style.textAlign = "right";
            div.innerHTML = data.val().data;
　　　　    
            parent.appendChild(div);
            Txt.value = "";
    });
}

window.onload = function() {
    init();
};