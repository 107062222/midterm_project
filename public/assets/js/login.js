function initApp() {
    
    firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
            constructdata();
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            document.getElementById("logout").addEventListener('click', function() {
                firebase.database().ref('talk/now/' + firebase.auth().currentUser.uid).set({
                    to: 000
                });
                firebase.auth().signOut().then(function(result) {
                    document.getElementById("me").href = "#Login";
                    window.location.hash="#home";
                    document.getElementById("begin").href = "#";
                    document.getElementById("hi").style.display = "none";
                }).catch(function(error) {
                    alert("logout error");
                });

            });
        } else {
            // It won't show any post if not login
            document.getElementById("me").href = "#Login";
            
        }
    });
    
    
    // Login with Email/Password
    var Email = document.getElementById("inputEmail");
    var Password = document.getElementById("inputPassword");
    var btnLogin = document.getElementById("btnLogin");
    var btnGoogle = document.getElementById("btngoogle");
    var btnSignUp = document.getElementById("btnSignUp");

    btnLogin.addEventListener('click', function() {
        firebase.auth().signInWithEmailAndPassword(Email.value, Password.value).then(function(result) {
            Email.value = '';
            Password.value = '';
        }).catch(function(error) { 
            alert("login fail:" + error.message);
            Password.value='';
        });
    });
    
    btnGoogle.addEventListener('click', function() {
        var provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth().signInWithPopup(provider).then(function(result) { 
            
           
        }).catch(function(error) { 
            alert("login fail:" + error.message);
            Password.value='';
        });
    });

    btnSignUp.addEventListener('click', function() {
        
        firebase.auth().createUserWithEmailAndPassword(Email.value, Password.value).then(function(result) {
            Email.value = '';
            Password.value = '';
        }).catch(function(error) { 
            alert("logup fail:" + error.message);
            Password.value='';
        });
    });
}
function constructdata(){
    
    var done = true;
    var query = firebase.database().ref('user/').orderByKey();
    firebase.database().ref('talk/now/' + firebase.auth().currentUser.uid).set({
        to: 000
    });
    query.once("value")
        .then(function(snapshot) {
            snapshot.forEach(function(childSnapshot) {
            // key will be "ada" the first time and "alan" the second time
            var key = childSnapshot.key;
            if(key == firebase.auth().currentUser.uid)
                done = false;
            });
            if(done){
                name();
            }else{
                console.log("U");
                addid()
            }
        });
}
function name(){
    nick = prompt("輸入你的暱稱:D");
    if(nick){
        changesay();
        alert(nick+"歡迎壓!");
        firebase.database().ref('user/' + firebase.auth().currentUser.uid).update({
            Nick: nick,
            name: firebase.auth().currentUser.email,
            pic: "image.png"
        });
        firebase.database().ref('talk/now/' + firebase.auth().currentUser.uid).set({
            to: 000
        });
        addid();
    }else{
        alert("人不能沒有名字ˋˊ");
        name();
    }
}
function addid(){

    window.location.hash="#connect";
    document.getElementById("me").href = "#connect";
    choice();
}

function Go(key){
    window.location.hash="#chat";
    document.getElementById("hi").style.display = "";
    chat(key);
    document.getElementById("begin").href = "#connect";
}

function choice(){
    var userRef = firebase.database().ref('user');
    var parent = document.getElementById("choice_list");
    
    var userId = firebase.auth().currentUser.uid;
    document.getElementById("homename").innerHTML = "";
    document.getElementById("homeemail").innerHTML = "";
    firebase.database().ref('user/' + userId).once('value').then(function(snapshot) {
        document.getElementById("homename").innerHTML = snapshot.val().Nick;
        document.getElementById("homeemail").innerHTML = snapshot.val().name;
        document.getElementById("homesay").innerHTML = snapshot.val().say;
        document.getElementById("homepic").src = "images/" + snapshot.val().pic;
    });
    

    parent.innerHTML = "";
    userRef.off();
    userRef.on('child_added', function(data) {
        if(data.val().name != firebase.auth().currentUser.email){
            var ct = document.createElement("contain");
            ct.className = "row people ";
            ct.title = data.key;
            ct.onclick =function(){
                Go(this.title)
                firebase.database().ref('talk/now/' + firebase.auth().currentUser.uid).set({
                    to: this.title,
                });
                firebase.database().ref('talk/msg/' + firebase.auth().currentUser.uid).child(this.title).remove().then(function() {
                    document.getElementById(data.key).style.display = "none";
                    console.log("Remove succeeded.")
                })
             };
            

            var img = document.createElement("img");
            img.src = "images/" + data.val().pic;
            img.className = "pimg";

            var div = document.createElement("div");
            div.className = "pname";
            div.innerHTML = data.val().Nick;

            var dot = document.createElement("div");
            dot.className = "pdot ";
            dot.setAttribute('id', data.key);
            dot.style.display = "none";

            ct.appendChild(img);
            ct.appendChild(div);
            ct.appendChild(dot);
            parent.appendChild(ct);
            
        } 
       
        var buttomRef = firebase.database().ref('talk/msg/' + firebase.auth().currentUser.uid);
        buttomRef.off();
        buttomRef.on('child_added', function(data){
            document.getElementById(data.key).style.display = "";
            if(window.Notification && Notification.permission === "granted"){
                console.log("okok");
                
                new Notification("您有來自-" + data.val().from + "-的未讀訊息", {
                    tag: 'newArrival'
                }); 
               
            }
        })
    });

    
}
function changesay(){
    var say = prompt("更改你的個簽");
    if(say){
        document.getElementById("homesay").innerHTML = say;
        firebase.database().ref('user/' + firebase.auth().currentUser.uid).update({
            say: say
        });
    }

}
function choicepicture(){
    window.location.hash="#work";
    document.getElementById("pict").style.display = "";
}
function changepicture(pic){
    firebase.database().ref('user/' + firebase.auth().currentUser.uid).update({
        pic: pic
    });
    document.getElementById("homepic").src = "images/" + pic;
    window.location.hash="#connect";
    document.getElementById("pict").style.display = "none";
}
function checklegal(word)  
{  
    word = word.replace(/&/g, '&amp;');
    word = word.replace(/</g, '&lt;');
    word = word.replace(/>/g, '&gt;');
    word = word.replace(/"/g, '&quot;');
    word = word.replace(/'/g, '&#039;');
    return word;
}
var Mail;
function chat(key){
    firebase.database().ref('user/' + key).once('value').then(function(snapshot) {
        nick = snapshot.val().Nick;
        Mail = snapshot.val().name;
        document.getElementById("chatheader").innerHTML = nick;
        document.getElementById("chatheader").title = snapshot.val().say;
    });
    document.getElementById("begin").href = "#connect";
    
    var Submit = document.getElementById("submit");
    var Txt = document.getElementById("txt");
    var user = firebase.auth().currentUser;
    
    Submit.addEventListener('click', function(){
        if(Txt.value != ""){
            
            firebase.database().ref('text/').push({
                from: firebase.auth().currentUser.email,
                to: Mail,
                data: checklegal(Txt.value)
            });
            firebase.database().ref('talk/now/' + firebase.auth().currentUser.uid).once('value').then(function(snapshot){
                var k = snapshot.val().to;
                firebase.database().ref('talk/now/' + k).once('value').then(function(snapshot1) {
                    console.log(k);
                    var To = snapshot1.val().to;
                    if(firebase.auth().currentUser.uid != To){
                        console.log("ok");
                        firebase.database().ref('user/' + firebase.auth().currentUser.uid).once('value').then(function(snapshot2) {
                            dt = new Date();
                            firebase.database().ref('talk/msg/' + k + '/' + firebase.auth().currentUser.uid).set({
                                time:　dt.getTime(),
                                unread: true,
                                from: snapshot2.val().Nick
                            });
                        });
                        /*dt = new Date();
                        firebase.database().ref('talk/msg/' + k + '/' + firebase.auth().currentUser.uid).set({
                            time:　dt.getTime(),
                            unread: true
                        });*/
                    }
                });
            })
            
            
            console.log(Mail);
            Txt.value = "";
        }
    })

    var postsRef = firebase.database().ref('text');
    var parent = document.getElementById("post_list");
    parent.innerHTML = "";
    postsRef.off();
    postsRef.on('child_added', function(data) {
            if(user.email == data.val().to && Mail == data.val().from){
                var div = document.createElement("div");
                div.style.width = "100%"

                var font = document.createElement("font");
                font.style.background = "black"
                font.style.border = "5px solid black";
                font.style.borderRadius = "25px 25px 25px 5px";
                font.style.borderImageSource = "url('images/border2.png')";
                font.style.borderImageSlice = "50%";
                font.style.borderImageOutset = "10px";
                font.style.borderImageWidth = "30px"

                font.style.marginTop = "15px";
                font.style.marginBottom = "15px"
                font.style.marginLeft = "15px";
                
                font.style.textAlign = "left"
                font.style.wordBreak = "break-all";
                font.style.color = "white";
                font.style.fontSize = "20px";
                
                font.style.width = "fit-content";
                font.style.maxWidth = "50%";
                font.style.float = "left";

                
                
                
                font.innerHTML = data.val().data;
                div.appendChild(font);
                parent.appendChild(div);
                parent.scrollTop = parent.scrollHeight;
            }
            else if(user.email == data.val().from && Mail == data.val().to){
                var div = document.createElement("div");
                div.style.width = "100%";

                var font = document.createElement("font");
                font.style.background = "black"
                font.style.border = "5px solid red";
                font.style.borderRadius = "25px 25px 5px 25px";
                font.style.borderImageSource = "url('images/border.png')";
                font.style.borderImageSlice = "50%";
                font.style.borderImageOutset = "20px";
                font.style.borderImageWidth = "40px"

                font.style.marginTop = "15px";
                font.style.marginBottom = "15px"
                font.style.marginRight = "15px";
                
                font.style.textAlign = "left"
                font.style.wordBreak = "break-all";
                font.style.color = "white";
                font.style.fontSize = "20px";
                
                font.style.width = "fit-content";
                font.style.maxWidth = "50%";
                font.style.float = "right";
                
                font.innerHTML = data.val().data;
                div.appendChild(font);
                parent.appendChild(div);
                parent.scrollTop = parent.scrollHeight;
            }
            
            
    });
    return;
}

window.onload = function() {
    firebase.auth().setPersistence(firebase.auth.Auth.Persistence.SESSION);
    if (Notification.permission === 'default' || Notification.permission === 'undefined') {
        Notification.requestPermission(function(permission) {
          // permission 可為「granted」（同意）、「denied」（拒絕）和「default」（未授權）
          // 在這裡可針對使用者的授權做處理
        });
    }
    initApp();
};
window.onbeforeunload = function() {
    firebase.database().ref('talk/now/' + firebase.auth().currentUser.uid).set({
        to: 000
    });
    alert("歡迎下次光臨!");   
}