# Software Studio 2020 Spring Midterm Project

## Topic
* Project Name : [Midterm-Project-107062222]
* Key functions (add/delete)
    1. 創1對1私人聊天室，並記錄目前和誰聊天
    2. 把傳的訊息、傳送者和接收者的數據放入database
    3. 下載歷史訊息，並檢查傳送者和接收者，決定是否放入當前的聊天視窗
    
* Other functions (add/delete)
    1. 一開始可以設定暱稱，之後可以自由選擇頭像和更改個簽
    2. 除了Chrome notification外，若有來自某人的未讀訊息，會在列表上，某人的欄位旁加紅點提醒

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|15%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|20%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

## Website Detail Description

# 作品網址：[https://midterm-project-107062222.web.app]

# Components Description : 
1. Membership Mechanism:
    Sign In & Up: 
    在剛進入的介面上，點擊飛機造型的icon(圖0)就會進入登入和註冊介面，可以用google帳號登錄，也可以用mail註冊帳號。
    若是按註冊或第一次用google帳號登錄，就會跳出輸入框，要求輸入暱稱和個簽，暱稱的輸入框不能空白，會彈出警告並再次跳出輸入框，直到輸入有效的暱稱，但個簽的輸入可以空白，因為之後能再改。
    
    Sign Out:
    若是上次已經登錄過，這次就會跳過登入介面直接進入主頁，主頁上有登出的按鈕(圖1)。
    
    ![](https://i.imgur.com/gUUoNyX.jpg)圖0
    ![](https://i.imgur.com/q871cKL.jpg)圖1
2. 個人主頁: 當狀態為登錄之後會進入主頁，在這裡可以選擇跟所有註冊過帳號的人聊天。
    (a)左側有一個名為大廳的選單(圖2)，裡面有所有人的頭像跟暱稱，點擊選單裡的選項，就會跳出1對1的私人聊天室了，當有人傳訊息給使用者，但使用者目前的聊天對象不是他或是使用者不再線時，會跳出chrome notification，通知使用者有來自XXX的未讀訊息。
    (b)右側會顯示自己的暱稱、mail、頭像和個簽。
    
    ![](https://i.imgur.com/ozrZQdF.jpg)圖2
    
    
3. 聊天視窗:
    進入聊天視窗之後，左上會顯示聊天對象，中間是聊天框，框的右側是使用者傳的訊息，左側是對方傳的訊息，底下有聊天輸入框，按飛機圖案的icon就能傳出訊息，當訊息太長時，最後顯示的訊息會自動換行，當屬標移到聊天對象的暱稱時，會顯示對方的個簽。
4. 點home的圖案(圖3)可以回主頁選擇其他聊天對象
    ![](https://i.imgur.com/lWRy6gI.jpg)圖3

5. CSS animation動畫的部分，左上的月亮會上下傾斜且背後的光會一閃一閃(圖4)、左下持續走動的鳳梨人(圖5)，還有視窗轉換時的浮現和滑動部分。
    ![](https://i.imgur.com/UcFSZyq.jpg)圖4
    
    ![](https://i.imgur.com/QcysKGi.jpg)圖5
# Other Functions Description : 
1. 暱稱、頭像更改:
    在主頁右側，點擊頭像會跳去選擇介面(圖6)，點擊選擇介面上的圖案就能更改頭像了,點擊個簽旁的設定圖案，就會跳出更改個簽的輸入框。
    
    ![](https://i.imgur.com/0pfuaMi.jpg)圖6
2. 未讀訊息:
    當有人傳訊息給使用者，但使用者目前的聊天對象不是他或是使用者不再線時，就會在大廳的選單上出現紅點(圖7)，表示有來自紅點位置所在人的未讀訊息，點擊之後紅點就會消失。
    ![](https://i.imgur.com/SqmUe96.jpg)圖7


## Security Report (Optional)
    在訊息放入database之前，會先被丟去一個function裡處理，把html的相關特殊符號轉成普通字元，我總共考慮了&、<、>、"、'這5種特殊符號。
